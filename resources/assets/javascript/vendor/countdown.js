//Countdown plugin

(function ($) {
  $.fn.countdown = function (options, callback) {

    //custom 'this' selector
    var thisEl = $(this);

    //array of custom settings
    var settings = {
      'date': null
    };

    //append the settings array to options
    if (options) {
      $.extend(settings, options);
    }

    //main countdown function
    var countdown_proc = function () {

      var eventDate = Date.parse(settings['date']) / 1000;
      var currentDate = Math.floor($.now() / 1000);

      if (eventDate <= currentDate) {
        callback.call(this);
        clearInterval(interval);
      }

      var seconds = eventDate - currentDate;

      var days = Math.floor(seconds / (60 * 60 * 24)); //calculate the number of days
      seconds -= days * 60 * 60 * 24; //update the seconds variable with no. of days removed

      var hours = Math.floor(seconds / (60 * 60));
      seconds -= hours * 60 * 60; //update the seconds variable with no. of hours removed

      var minutes = Math.floor(seconds / 60);
      seconds -= minutes * 60; //update the seconds variable with no. of minutes removed

      //logic for the two_digits mode
      minutes = (String(minutes).length >= 2) ? minutes : "0" + minutes;
      seconds = (String(seconds).length >= 2) ? seconds : "0" + seconds;
      var minutes_1 = minutes.toString().substring(0, 1);
      var minutes_2 = minutes.toString().substring(1, 2);
      var seconds_1 = seconds.toString().substring(0, 1);
      var seconds_2 = seconds.toString().substring(1, 2);

      //update the countdown's html values.
      if (!isNaN(eventDate)) {
        thisEl.find(".minutes-1").text(minutes_1);
        thisEl.find(".minutes-2").text(minutes_2);
        thisEl.find(".seconds-1").text(seconds_1);
        thisEl.find(".seconds-2").text(seconds_2);
      } else {
        alert("Invalid date. Error #12032");
        clearInterval(interval);
      }
    };

    //run the function
    countdown_proc();

    //loop the function
    interval = setInterval(countdown_proc, 1000);

  }
})(jQuery);