import $ from 'jquery';
require('./vendor/countdown');

class Application{
    constructor(){
        console.log('application start');
        document.addEventListener('DOMContentLoaded', () => {
            this._init();
        })
    }

    _init(){
        this._countdown();
    }

    _countdown(){
        $(".countdown").countdown({
                date: "2017-07-23T15:40:00" // countdown's end date (i.e. 3 november 2012 12:00:00 or 2012-11-03T12:00:00)
        },
        function () {
            // the code here will run when the countdown ends
            alert("done!")
        });
    }
}

new Application();