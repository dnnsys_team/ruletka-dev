@extends('layouts/layout')

@section('content')
    <div class="page-title-section">
        <h1 class="page-title">Рулетка</h1>
        <h2 class="page-title-caption">ГАРАНТИРОВАННЫЙ ВОЗВРАТ ДЕНЕГ ТОП ДОНАТЕРУ</h2>
    </div><!-- .page-title-section -->
    <div class="roulette-title-section">
        <div class="roulette-title-top-border"></div>
        <div class="roulette-title-container">
            <svg class="roulette-title-triangle-left" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 40 74" enable-background="new 0 0 40 74">
                <linearGradient id="triangle_gradient_2" gradientUnits="userSpaceOnUse" x1="20" y1="74" x2="20" y2="3.668511e-07" gradientTransform="matrix(-1 0 0 1 40 0)">
                    <stop offset="0" style="stop-color:#015E45"></stop>
                    <stop offset="1" style="stop-color:#014231"></stop>
                </linearGradient>
                <polygon fill="url(#triangle_gradient_2)" points="0,0 40,0 40,74 "></polygon>
            </svg>
            <svg class="roulette-title-triangle-right" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 40 74" enable-background="new 0 0 40 74">
                <polygon fill="url(#triangle_gradient_2)" points="40,0 0,0 0,74 "></polygon>
            </svg>
            <div class="roulette-title">
                <div class="roulette-title-left">
                    <a href="#">История игр</a>
                </div>
                <div class="roulette-title-center">МИНИМАЛЬНАЯ СТАВКА <span>100</span> РУБЛЕЙ</div>
                <div class="roulette-title-right">
                    <a href="#">Правила игры</a>
                </div>
            </div><!-- .roulette-title -->
        </div>
    </div><!-- .roulette-title-section -->
    <div class="match-info">
        <div class="match-number">ИГРА <span>#27805</span></div>
        <div class="match-bank">БАНК: <span>22000</span>&#8381;</div>
        <div class="match-info-alien-head"></div>
    </div><!-- .match-info -->
    <div class="roulette-slider-section">
        <div class="roulette-slider">
            <div class="roulette-slider-inner">
                <div class="roulette-player"><img src="images/avatar/1.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/2.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/3.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/15.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/15.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/2.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/5.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/6.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/8.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/5.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/6.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/7.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/2.jpg" alt="User Name"></div>
                <div class="roulette-player"><img src="images/avatar/3.jpg" alt="User Name"></div>
            </div>
        </div><!-- .roulette-slider -->
        <div class="roulette-slider-arrow"></div>
        <div class="roulette-slider-divider"></div>
    </div><!-- .roulette-slider-section -->
    <div class="roulette-start-counter-section">
        <div class="roulette-start-progressbar-container">
            <div class="roulette-start-progressbar-outer">
                <div class="roulette-start-progressbar-inner"></div>
                <div class="roulette-start-progressbar-title">30/50</div>
                <div class="roulette-start-progressbar-title-caption">ВЕЩЕЙ</div>
            </div>
        </div><!-- .roulette-start-progressbar-container -->
        <div class="roulette-start-countdown-container">
            <div class="countdown-title">ИЛИ ЧЕРЕЗ</div>
            <div class="countdown">
                <div class="countdown-number-slot"><span class="minutes-1"></span></div>
                <div class="countdown-number-slot"><span class="minutes-2"></span></div>
                <div class="countdown-number-divider">:</div>
                <div class="countdown-number-slot"><span class="seconds-1"></span></div>
                <div class="countdown-number-slot"><span class="seconds-2"></span></div>
            </div>
        </div><!-- .roulette-start-countdown-container -->
    </div><!-- .roulette-start-counter-section -->
    <div class="my-bet-section">
        <div>
            <div class="my-bet-title">Вы вложили в игру - <span class="my-bet-amount">0</span> (из
                <span class="max-bet-amount-allowed">10</span>) вещей
            </div>
            <div class="my-bet-title-caption">Чем выше ставка, тем больше шанс победить.</div>
        </div>
        <div class="my-chance">
            <div class="my-chance-title">Шанс <br>выигрыша:</div>
            <div class="my-chance-percent">0%</div>
        </div>
        <div class="make-a-bet-button-container">
            <a href="#" class="make-a-bet-button">ВЛОЖИТЬ ПРЕДМЕТ</a>
        </div>
    </div>
    <div class="all-players-section">
        <div class="roulette-title-top-border"></div>
        <div class="all-players">
            <div class="all-players-inner">
                <div class="roulette-player">
                    <img src="images/avatar/1.jpg" alt="User Name">
                    <div class="roulette-player-chance">72.3%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/2.jpg" alt="User Name">
                    <div class="roulette-player-chance">8.3%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/3.jpg" alt="User Name">
                    <div class="roulette-player-chance">3.3%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/4.jpg" alt="User Name">
                    <div class="roulette-player-chance">1.3%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/5.jpg" alt="User Name">
                    <div class="roulette-player-chance">0.32%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/6.jpg" alt="User Name">
                    <div class="roulette-player-chance">0.22%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/7.jpg" alt="User Name">
                    <div class="roulette-player-chance">0.22%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/8.jpg" alt="User Name">
                    <div class="roulette-player-chance">0.22%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/9.jpg" alt="User Name">
                    <div class="roulette-player-chance">0.22%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/10.jpg" alt="User Name">
                    <div class="roulette-player-chance">0.22%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/11.jpg" alt="User Name">
                    <div class="roulette-player-chance">0.22%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/12.jpg" alt="User Name">
                    <div class="roulette-player-chance">0.22%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/13.jpg" alt="User Name">
                    <div class="roulette-player-chance">0.22%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/14.jpg" alt="User Name">
                    <div class="roulette-player-chance">0.22%</div>
                </div>
                <div class="roulette-player">
                    <img src="images/avatar/15.jpg" alt="User Name">
                    <div class="roulette-player-chance">0.22%</div>
                </div>
            </div><!-- .all-players-inner -->
        </div><!-- .all-players -->
        <div class="roulette-title-bottom-border"></div>
        <div class="all-players-arrow-left"><span>&#9204;</span></div>
        <div class="all-players-arrow-right"><span>&#9205;</span></div>
    </div><!-- .all-players-section -->
    <div class="all-bets-section">
        <div class="bet-container">
            <div class="user-avatar"><img src="images/avatar/14.jpg" alt="User Name"></div>
            <div class="user-name">Player вложил</div>
            <div class="bet-amount">1600 руб.</div>
            <div class="bet-item-name">KERAMBIT | GAMMA DOPPLER (НЕМНОГО ПОНОШЕННЫЙ)</div>
            <div class="tickets">Билеты: #368 - #1892</div>
            <div class="bet-item-image-background" style="background: linear-gradient(to bottom, #1a1a1a 0%, #421818 100%);"></div>
            <div class="bet-item-image" style="background-image: url(images/gun-1.png);"></div>
        </div>
        <div class="bet-container">
            <div class="user-avatar"><img src="images/avatar/15.jpg" alt="User Name"></div>
            <div class="user-name">Player вложил</div>
            <div class="bet-amount">1600 руб.</div>
            <div class="bet-item-name">KERAMBIT | GAMMA DOPPLER (НЕМНОГО ПОНОШЕННЫЙ)</div>
            <div class="tickets">Билеты: #368 - #1892</div>
            <div class="bet-item-image-background" style="background: linear-gradient(to bottom, #1a1a1a 0%, #16649a 100%);"></div>
            <div class="bet-item-image" style="background-image: url(images/gun-2.png);"></div>
        </div>
        <div class="bet-container">
            <div class="user-avatar"><img src="images/avatar/3.jpg" alt="User Name"></div>
            <div class="user-name">Player вложил</div>
            <div class="bet-amount">1600 руб.</div>
            <div class="bet-item-name">KERAMBIT | GAMMA DOPPLER (НЕМНОГО ПОНОШЕННЫЙ)</div>
            <div class="tickets">Билеты: #368 - #1892</div>
            <div class="bet-item-image-background" style="background: linear-gradient(to bottom, #1a1a1a 0%, #42a281 100%);"></div>
            <div class="bet-item-image" style="background-image: url(images/gun-3.png);"></div>
        </div>
        <div class="bet-container">
            <div class="user-avatar"><img src="images/avatar/8.jpg" alt="User Name"></div>
            <div class="user-name">Player вложил</div>
            <div class="bet-amount">1600 руб.</div>
            <div class="bet-item-name">KERAMBIT | GAMMA DOPPLER (НЕМНОГО ПОНОШЕННЫЙ)</div>
            <div class="tickets">Билеты: #368 - #1892</div>
            <div class="bet-item-image-background" style="background: linear-gradient(to bottom, #1a1a1a 0%, #033d5e 100%);"></div>
            <div class="bet-item-image" style="background-image: url(images/gun-4.png);"></div>
        </div>
        <div class="bet-container">
            <div class="user-avatar"><img src="images/avatar/4.jpg" alt="User Name"></div>
            <div class="user-name">Player вложил</div>
            <div class="bet-amount">1600 руб.</div>
            <div class="bet-item-name">KERAMBIT | GAMMA DOPPLER (НЕМНОГО ПОНОШЕННЫЙ)</div>
            <div class="tickets">Билеты: #368 - #1892</div>
            <div class="bet-item-image-background" style="background: linear-gradient(to bottom, #1a1a1a 0%, #7bc100 100%);"></div>
            <div class="bet-item-image" style="background-image: url(images/gun-5.png);"></div>
        </div>
        <div class="bet-container">
            <div class="user-avatar"><img src="images/avatar/7.jpg" alt="User Name"></div>
            <div class="user-name">Player вложил</div>
            <div class="bet-amount">1600 руб.</div>
            <div class="bet-item-name">KERAMBIT | GAMMA DOPPLER (НЕМНОГО ПОНОШЕННЫЙ)</div>
            <div class="tickets">Билеты: #368 - #1892</div>
            <div class="bet-item-image-background" style="background: linear-gradient(to bottom, #1a1a1a 0%, #4f00f4 100%);"></div>
            <div class="bet-item-image" style="background-image: url(images/gun-6.png);"></div>
        </div>
        <div class="bet-container roulette-started">
            <div>SHA224 хэш для этого раунда:</div>
            <div>cdc20434e0aba2a4fd4b963959452247f1224c9ed4178956afe4a258</div>
            <a href="#">Что это?</a>
        </div>
        <div class="roulette-title-bottom-border"></div>
    </div><!-- .all-bets-section -->
    <div class="best-players-of-the-day-section">
        <div class="last-winner">
            <div class="best-player-of-the-day-title">ПОСЛЕДНИЙ ПОБЕДИТЕЛЬ</div>
            <div class="best-player-of-the-day-container">
                <div class="best-player-of-the-day-avatar"><img src="images/avatar/5.jpg" alt="User Name"></div>
                <div class="best-player-of-the-day-name">Vladysha1337</div>
            </div>
            <div class="best-player-stats-container">
                <table class="best-player-stats-table">
                    <tr>
                        <td>Выигрыш:</td>
                        <td>128865 руб.</td>
                    </tr>
                    <tr>
                        <td>Шанс:</td>
                        <td>52.9%</td>
                    </tr>
                </table>
            </div>
        </div><!-- .last-winner -->
        <div class="lucky-of-the-day">
            <div class="best-player-of-the-day-title">СЧАСТЛИВЧИК ДНЯ</div>
            <div class="best-player-of-the-day-container">
                <div class="best-player-of-the-day-avatar"><img src="images/avatar/4.jpg" alt="User Name"></div>
                <div class="best-player-of-the-day-name">Kidovskyi_Alex</div>
            </div>
            <div class="best-player-stats-container">
                <table class="best-player-stats-table">
                    <tr>
                        <td>Выигрыш:</td>
                        <td>51671 руб.</td>
                    </tr>
                    <tr>
                        <td>Шанс:</td>
                        <td>0.32%</td>
                    </tr>
                </table>
            </div>
        </div><!-- .lucky-of-the-day -->
        <div class="the-biggest-bet">
            <div class="best-player-of-the-day-title">НАИБОЛЬШАЯ СТАВКА</div>
            <div class="best-player-of-the-day-container">
                <div class="best-player-of-the-day-avatar"><img src="images/avatar/15.jpg" alt="User Name"></div>
                <div class="best-player-of-the-day-name">Staryi_Skl9r</div>
            </div>
            <div class="best-player-stats-container">
                <table class="best-player-stats-table">
                    <tr>
                        <td>Выигрыш:</td>
                        <td>128865 руб.</td>
                    </tr>
                </table>
            </div>
        </div><!-- .the-biggest-bet -->
    </div><!-- .best-players-of-the-day-section -->
@stop